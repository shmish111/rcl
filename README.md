# Riemann Command Line Client (rcl)

A tiny command line client for streaming Riemann events.

## Use

`rcl --help`

The most basic command is

`rcl`

This will connect to a riemannn websocket server on `127.0.0.1` port `5556` with the query `true` i.e. it will stream all events going into Riemann.

If we want to connect to a remote server:

`rcl --host 1.2.3.4`

If we are using a non-default port:

`rcl --port 1234`

Really though, what we want to do is use an interesting query. Let's subscribe to Riemann's internal events only:

`rcl 'service =~ "riemann %"'`

For something more complicated we can use a HEREDOC:

```
rcl <<HERE '
service =~ "riemann streams %"
and not service = "riemann streams rate"
'
HERE
```

Finally, since events are streamed as json maps, we can combine with `jq`, let's filter so that we only see the service field:

`rcl 'service =~ "riemann %"' | jq '.service'`
