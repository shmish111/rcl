{ nixpkgs ? <nixpkgs>
, declInput ? {
    uri = "https://gitlab.com/shmish111/rcl.git";
    rev = "refs/heads/master";
  }
}:
let pkgs = import nixpkgs {};

    mkGitSrc = { repo, branch ? "refs/heads/master", deepClone ? false }: {
      type = "git";
      value = repo + " " + branch + (if deepClone then " deepClone" else "");
      emailresponsible = false;
    };

    mkJob = { name, description, nixexprinput ? "jobsetSrc", nixexprpath, extraInputs }: {
      inherit name;
      value = {
        inherit description nixexprinput nixexprpath;

        inputs = {
          jobsetSrc = mkGitSrc {
            repo = declInput.uri;
            branch = declInput.rev;
          };

          nixpkgs = mkGitSrc {
            repo = "https://github.com/NixOS/nixpkgs-channels";
            branch = "refs/heads/nixos-18.03";
          };
        } // extraInputs;

        enabled = 1;
        hidden = false;
        checkinterval = 90;
        schedulingshares = 100;
        emailoverride = "";
        enableemail = false;
        keepnr = 3;
      };
    };

    mkRclJob = { name, description, rclBranch }:
      mkJob {
        inherit name description;
        nixexprpath = "jobsets/release-rcl.nix";
        extraInputs = {
          rclSrc = mkGitSrc {
            repo = "https://gitlab.com/shmish111/rcl.git";
            branch = rclBranch;
            deepClone = true;
          };
        };
      };

    rclJobsetDefinition = pkgs.lib.listToAttrs (
      [
        (mkRclJob {
          name = "master";
          description = "master";
          rclBranch = "refs/heads/master";
        })
      ]
    );

    jobsetDefinition = rclJobsetDefinition;
in {
  jobsets = pkgs.runCommand "spec.json" {} ''
    cat <<EOF
    ${builtins.toXML declInput}
    EOF

    tee $out <<EOF
    ${builtins.toJSON jobsetDefinition}
    EOF
  '';
}
