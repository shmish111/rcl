{ nixpkgs ? <nixpkgs>
, rclSrc ? ../.
}:
let pkgs = import nixpkgs {};
in
{
  inherit (pkgs.callPackage ../default.nix {
    inherit rclSrc pkgs;
  }) rcl;
}
