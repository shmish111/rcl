{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Monad          (forever)
import           Control.Monad.IO.Class (liftIO)
import           Data.Text              (Text)
import qualified Data.Text.IO           as T
import           Network.Socket         (withSocketsDo)
import qualified Network.URI.Encode     as URI
import qualified Network.WebSockets     as WS
import           Options.Applicative    (Parser, argument, auto, execParser,
                                         fullDesc, header, help, helper, info,
                                         long, metavar, option, progDesc, short,
                                         str, strOption, value, (<**>))

data Spec = Spec
  { host  :: String
  , port  :: Int
  , query :: String
  }

specParser :: Parser Spec
specParser = Spec
  <$> strOption ( long "host" <> short 'a' <> metavar "HOST" <> help "Riemann Server Host" <> value "127.0.0.1" )
  <*> option auto ( long "port" <> short 'p' <> metavar "PORT" <> help "Riemann Server Port" <> value 5556 )
  <*> argument str ( metavar "QUERY" <> help "Riemann Subscription Query" <> value "true" )

app :: WS.ClientApp ()
app conn = forever $ do
  msg <- WS.receiveData conn
  liftIO . T.putStrLn $ msg

mkPath :: String -> String
mkPath q = "/index?subscribe=true&query=" <> (URI.encode q)

run :: Spec -> IO ()
run (Spec host port query) = withSocketsDo $ WS.runClient host port (mkPath query) app

main :: IO ()
main = run =<< execParser opts
  where
    opts = info (specParser <**> helper)
      ( fullDesc
      <> progDesc "Subscribe to a Riemann websocket"
      <> header "rcl - Riemann Command Line Client"
      )
